console.log(`Hello World`);

function printUserInfo(fname, lname, age, hobbies, address){
	console.log(fname);
	console.log(lname);
	console.log(age);
	console.log(hobbies);
	console.log(address);
}

printUserInfo(`First Name: John`, `Last name: Smith`, `Age: 30`,
	['Biking', 'Mountain Climbing', 'Swimming'], {
		houseNumber: `32`,
		street: `Washington`,
		city: `Lincoln`,
		state: `Nebrask`
	});

printUserInfo(`John Smith is 30 years of age.`, `This was printed inside of the function`, [`Biking`, `Mountain Climbing`, `Swimming`], `This was printed inside of the function`, 
	{
		houseNumber: `32`,
		street: `Washington`,
		city: `Lincoln`,
		state: `Nebrask`
	});

function returnFunction(isMarried){
	return isMarried = true;
}

let result = `The value of isMarried is: `;
console.log(`${result}` +  returnFunction());